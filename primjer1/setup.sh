#!/bin/bash

echo "GENERAL APT-GET UPDATE"
# -qq implies -y --force-yes
apt-get -qq update

echo "INSTALL GIT"
apt-get -qq install git

echo "INSTALL VIM"
apt-get -qq install vim

echo "INSTALL CURL"
apt-get -qq install curl

echo "INSTALL python-software-properties"
apt-get -qq install python-software-properties

echo "INSTALL php7 nginx"
apt-get -qq install nginx
apt-get -qq install php7.0-fpm
apt-get -qq install php7.0-cli
apt-get -qq install php7.0-curl

echo "START NGINX"
service nginx start

echo "Done!"
